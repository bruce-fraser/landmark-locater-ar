//
//  BGFPlaceItem.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/4/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import Foundation; import MapKit

class BGFPlaceItem: NSObject, NSSecureCoding {
    static var supportsSecureCoding: Bool {
        return true
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(loadedAs.rawValue, forKey: "loadedAs")
        aCoder.encode(iconType.rawValue, forKey: "iconType")
        aCoder.encode(favorited, forKey: "favorited")
        aCoder.encode(showSign, forKey: "showSign")
        aCoder.encode(mapItem, forKey: "mapItem")
        aCoder.encode(createdAt, forKey: "createdAt")
        aCoder.encode(altitude, forKey: "altitude")
    }
    required convenience init?(coder aDecoder: NSCoder) { print("reched here")
        let la = aDecoder.decodeObject(forKey: "loadedAs") as! String
        let iType = aDecoder.decodeObject(forKey: "iconType") as! String
        let fav = aDecoder.decodeBool(forKey: "favorited")
        let ss = aDecoder.decodeBool(forKey: "showSign")
        let mi = aDecoder.decodeObject(forKey: "mapItem") as! MKMapItem
        let cd = aDecoder.decodeObject(forKey: "createdAt") as! Date
        let alt = aDecoder.decodeDouble(forKey: "altitude")
        let la2 = LoadedType(rawValue: la)!
        let iconType = IconType(rawValue: iType)!
        self.init(loadedAs: la2, isFavorited: fav, with: mi, cDate: cd, showSign: ss, iconType: iconType, altitude: alt)
    }
    enum LoadedType: String {
        case generalPlace
        case favorite
        case searchResult
        case mySpot
        case receivedSpot
        
        var color: UIColor {
            switch self {
            case .generalPlace, .favorite: return UIColor(named: "generalPlaceColor")!
            case .searchResult: return UIColor.orange
            case .mySpot: return UIColor(named: "mySpotColor")!
            case .receivedSpot: return UIColor(named: "receivedSpotColor")!
            }
        }
    }
    enum IconType: String, CaseIterable, Codable, Comparable {
        case bawling, confused, cube, dizzy, frog, grimace, happy, horse, love, rage, shark, smirk, sphere, surprised, windmill, worried
        
        var yValue: Float {
            switch self {
            case .windmill: return 12.0
            case .shark, .horse: return 3.0
            default: return 2.0
            }
        }
        
        public static func < (a: IconType, b: IconType) -> Bool {
            return a.rawValue < b.rawValue
        }
    }
    
    var iconType: IconType
    var loadedAs: LoadedType
    var favorited: Bool
    var delete: Bool
    var showSign: Bool
    var mapItem: MKMapItem
    var createdAt: Date
    var altitude: Double
    
    init(loadedAs: LoadedType, isFavorited favorited: Bool, with mi: MKMapItem, cDate: Date = Date(), showSign: Bool = true, iconType: IconType = .sphere, delete: Bool = false, altitude: Double = 0.0) {
        self.loadedAs = loadedAs
        self.favorited = favorited
        self.mapItem = mi
        self.createdAt = cDate
        self.showSign = showSign
        self.iconType = iconType
        self.delete = delete
        self.altitude = altitude
        
        super.init()
    }
}

extension BGFPlaceItem: MKAnnotation {
    var coordinate: CLLocationCoordinate2D {
        get { return mapItem.placemark.coordinate }
    }
    var title: String? {
        get {
            if let s = mapItem.name, s.count > 12 {
                return String(s.prefix(12))
            }
            return mapItem.name
        }
    }
    var subtitle: String? {
        get { return nil }
    }
}
