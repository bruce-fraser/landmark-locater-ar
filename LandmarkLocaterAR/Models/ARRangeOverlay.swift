//
//  ARRangeOverlay.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/12/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import MapKit

class ARRangeOverlay: NSObject, MKOverlay {
    var coordinate: CLLocationCoordinate2D
    var boundingMapRect: MKMapRect
    
    init(_ coord: CLLocationCoordinate2D) {
        self.coordinate = coord;
        self.boundingMapRect = MKMapRect(origin: MKMapPoint(coord), size: MKMapSize(width: 11700, height: 11700))
    }
}
