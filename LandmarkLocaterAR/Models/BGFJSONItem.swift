//
//  BGFJSONItem.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/26/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import Foundation

struct BGFJSONItem: Codable {
    var lat: Double
    var lon: Double
    var alt: Double
    var name: String
    var showSign: Bool
    var iconType: BGFPlaceItem.IconType
    
    init(lat: Double, lon: Double, it: BGFPlaceItem.IconType, alt: Double, showSign: Bool, name: String) {
        self.lat = lat; self.lon = lon; self.iconType = it; self.alt = alt; self.showSign = showSign; self.name = name
    }
    
    init(bgfPlaceItem: BGFPlaceItem) {
        self.lat = bgfPlaceItem.coordinate.latitude; self.lon = bgfPlaceItem.coordinate.longitude
        self.alt = bgfPlaceItem.altitude; self.name = bgfPlaceItem.mapItem.name ?? ""
        self.showSign = bgfPlaceItem.showSign; self.iconType = bgfPlaceItem.iconType
    }
}
