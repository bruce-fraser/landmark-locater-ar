//
//  MySpotsProducts.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 3/31/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import Foundation

public struct MySpotsProducts {
    
    static let productIDsNonConsumables: Set<String> = [
        "com.brucefraser.LandmarkLocaterAR.iap.bawling",
        "com.brucefraser.LandmarkLocaterAR.iap.confused",
        "com.brucefraser.LandmarkLocaterAR.iap.dizzy",
        "com.brucefraser.LandmarkLocaterAR.iap.frog",
        "com.brucefraser.LandmarkLocaterAR.iap.grimace",
        "com.brucefraser.LandmarkLocaterAR.iap.happy",
        "com.brucefraser.LandmarkLocaterAR.iap.horse",
        "com.brucefraser.LandmarkLocaterAR.iap.rage",
        "com.brucefraser.LandmarkLocaterAR.iap.surprised",
        "com.brucefraser.LandmarkLocaterAR.iap.windmill",
        "com.brucefraser.LandmarkLocaterAR.iap.worried"
    ]
    
    public static func resourceName(for productIdentifier: String) -> String? {
        return productIdentifier.components(separatedBy: ".").last
    }
    
    static let store = IAPHelper(productIDs: MySpotsProducts.productIDsNonConsumables)
    
    static func handlePurchase(purchaseIdentifier: String) {
        if productIDsNonConsumables.contains(purchaseIdentifier) {
            store.purchasedProducts.insert(purchaseIdentifier)
            UserDefaults.standard.set(true, forKey: purchaseIdentifier)
        }
    }
}
