//
//  DataStore.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/2/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import Foundation; import MapKit

protocol DataStoreDelegate: class {
    func didFetchPlaces()
    func addedReceivedSpot(bP: BGFPlaceItem)
}
protocol MapDisplayDelegate: class {
    func didFetchPlaces(recenter: Bool)
    func addedReceivedPlace(place: BGFPlaceItem)
}

class DataStore {
    static let shared = DataStore()
    private let internalQueue = DispatchQueue(label: "com.brucefraser.landmarklocater.dataqueue", attributes: .concurrent)
    public let customSerialQueue = DispatchQueue(label: "com.bruce.fraser.landmarklocater.serialqueue")
    weak var delegate: DataStoreDelegate?
    weak var mapDisplayDelegate: MapDisplayDelegate?
    public var availableIcons: Set<String> {
        let purchasedNames = MySpotsProducts.store.purchasedProducts.compactMap { $0.components(separatedBy: ".").last }
        return Set(FREE_MODELS).union(purchasedNames)
    }
    
    private var _bgfPlaceItems: [BGFPlaceItem]?
    var bgfPlaceItems: [BGFPlaceItem]? {
        get { return internalQueue.sync { _bgfPlaceItems } }
        set (newState) { internalQueue.async(flags: .barrier) { self._bgfPlaceItems = newState } }
    }
    
//    private var _searchActive: Bool = false
//    var searchActive: Bool {
//        get { return internalQueue.sync { _searchActive } }
//        set (newState) { internalQueue.async(flags: .barrier) { self._searchActive = newState } }
//    }
    
    private var _displayingSearchResults: Bool = false
    var displayingSearchResults: Bool {
        get { return internalQueue.sync { _displayingSearchResults } }
        set (newState) { internalQueue.async(flags: .barrier) { self._displayingSearchResults = newState } }
    }
    
    private var _showGeneralPlacesFlag: Bool = false
    var showGeneralPlacesFlag: Bool {
        get { return internalQueue.sync { _showGeneralPlacesFlag } }
        set (newState) { internalQueue.async(flags: .barrier) { self._showGeneralPlacesFlag = newState } }
    }
    
    private var _searchTermsArray: [String] = ["school", "bank", "coffee", "restaurant", "gas"]
    var searchTermsArray: [String] {
        get { return internalQueue.sync { _searchTermsArray } }
        set (newState) { internalQueue.async(flags: .barrier) { self._searchTermsArray = newState } }
    }
    
    private init() {
        let spFlag = UserDefaults.standard.bool(forKey: "showGeneralPlacesFlag")
        showGeneralPlacesFlag = spFlag
        if let stArray = UserDefaults.standard.object(forKey: "searchTermsArray") as? [String] {
            searchTermsArray = stArray
        }
    }
    
    func retrieveItems() -> [BGFPlaceItem]? {
        if let items = UserDefaults.standard.data(forKey: "savedItems") {
            let items2 = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(items) as? [BGFPlaceItem]
            print("BGF --- favorites loaded .........")
            return items2
        }
        return nil
    }
    
    func storeItems() {
         print("BGF --- storeItems called")
        if let pi = bgfPlaceItems {
            let mi = pi.filter { ($0.favorited || $0.loadedAs == .mySpot || $0.loadedAs == .receivedSpot) && $0.delete == false }
            if let data = try? NSKeyedArchiver.archivedData(withRootObject: mi, requiringSecureCoding: false) {
                UserDefaults.standard.set(data, forKey: "savedItems")
            }
        }
        UserDefaults.standard.set(showGeneralPlacesFlag, forKey: "showGeneralPlacesFlag")
        UserDefaults.standard.set(searchTermsArray, forKey: "searchTermsArray")
    }
    
    func processNewItems(_ newItems: [MKMapItem]?, of type: BGFPlaceItem.LoadedType = .generalPlace, url: URL? = nil) {
        var items = bgfPlaceItems ?? [BGFPlaceItem]()
        if bgfPlaceItems == nil {
            if let bgfItems = retrieveItems() {
                items.append(contentsOf: bgfItems)
            }
        }
        
        switch type {
        case .generalPlace:
            items = items.filter { $0.favorited || $0.loadedAs == .mySpot || $0.loadedAs == .searchResult  || $0.loadedAs == .receivedSpot}
            if let nI = newItems {
                for n in nI {
                    if items.contains(where: { (bI) -> Bool in return bI.mapItem == n }) {
                        continue
                    } else {
                        let bgItem = BGFPlaceItem(loadedAs: .generalPlace, isFavorited: false, with: n)
                        items.append(bgItem)
                    }
                }
            }
        case .searchResult:
            let items2 = items.filter { $0.loadedAs != .searchResult && !$0.favorited }
            items = items.filter { $0.favorited }
            if let nI = newItems {
                displayingSearchResults = true
                for n in nI {
                    if items.contains(where: { (bI) -> Bool in return bI.mapItem == n }) {
                        continue
                    } else {
                        let bgItem = BGFPlaceItem(loadedAs: .searchResult, isFavorited: false, with: n)
                        items.append(bgItem)
                    }
                }
            } else {
                displayingSearchResults = false
            }
            for i in items2 {
                if items.contains(where: { (bI) -> Bool in bI.mapItem == i.mapItem }) {
                    continue
                } else {
                    items.append(i)
                }
            }
        case .receivedSpot:
            print("BGF ............. entered received spot switch")
            if let url = url, let data = try? Data(contentsOf: url), let rSpotJson = try? JSONDecoder().decode(BGFJSONItem.self, from: data) {
                let coord = CLLocationCoordinate2DMake(rSpotJson.lat, rSpotJson.lon); let pm = MKPlacemark(coordinate: coord)
                let mi = MKMapItem(placemark: pm); mi.name = rSpotJson.name
                let bgfPI = BGFPlaceItem(loadedAs: .receivedSpot, isFavorited: false, with: mi, cDate: Date(), showSign: rSpotJson.showSign, iconType: rSpotJson.iconType, delete: false, altitude: rSpotJson.alt)
                var bI: BGFPlaceItem?
                if let fItem = items.filter ({ $0.loadedAs == .receivedSpot && $0.mapItem == bgfPI.mapItem }).first {
                    bI = fItem
                } else {
                    items.append(bgfPI)
                    bI = bgfPI
                }
                bgfPlaceItems = items
                try? FileManager.default.removeItem(at: url)
                if let bi = bI {
                    mapDisplayDelegate?.addedReceivedPlace(place: bi)
                    delegate?.addedReceivedSpot(bP: bi)
                }
                return
            }
            if let url = url {
                try? FileManager.default.removeItem(at: url)
            }
            // not able to process .mspt file HUD
            print("BGF --- not able to process .mspt file")
            return
        default: return
        }
        print("BGF --- before delegate fetch places in process new items")
        bgfPlaceItems = items.count > 0 ? items: nil
        delegate?.didFetchPlaces()
        mapDisplayDelegate?.didFetchPlaces(recenter: true)
    }
    deinit {
        storeItems()
    }
}
