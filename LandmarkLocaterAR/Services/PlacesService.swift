//
//  PlacesService.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 1/19/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import Foundation
import MapKit

class PlacesService {
    static let service = PlacesService()
    public var errorString: String?
    
    private init () {}
    
    public func search(for queryStrings: [String] = DataStore.shared.searchTermsArray, coordinate: CLLocationCoordinate2D) {
        print("BGF --- top of search for")
        var searchRequests = [MKLocalSearch.Request]()
        queryStrings.forEach { (qs) in
            let sr = MKLocalSearch.Request(); sr.naturalLanguageQuery = qs
            sr.region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 2500, longitudinalMeters: 2500)
            searchRequests.append(sr)
        }
        search(using: searchRequests)
    }
    
    private func search(using searchRequests: [MKLocalSearch.Request]) {
        print("BGF --- top of search using")
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        var MIArray = [MKMapItem](); var errors = "";
        let dg = DispatchGroup()
        searchRequests.forEach { (sr) in
            dg.enter()
            MKLocalSearch(request: sr).start { (response, error) in
                guard error == nil else {
                    if let error = error as NSError?, let eS = error.userInfo[NSLocalizedDescriptionKey] as? String {
                        errors += eS + "  "
                    }
                    return
                }
                if let mi = response?.mapItems { MIArray += mi }
                dg.leave()
            }
        }
        
        dg.notify(queue: DataStore.shared.customSerialQueue) {
            DispatchQueue.main.async {
                 UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            print("BGF --- dispatch group notify; count with duplicates: \(MIArray.count)")
            print("BGF --- dispatch goup notify errors if any: \(errors)")
            self.errorString = errors == "" ? nil: errors
            if MIArray.count == 0 {
                DataStore.shared.processNewItems(nil)
                return
            }
            var miArray2  = [MKMapItem]()
            for m in MIArray {
                if miArray2.contains(where: { (sAItem) -> Bool in return sAItem == m }) {
                    continue
                }
                miArray2.append(m)
            }
            print("BGF --- dispatch group notify; count without duplicates \(miArray2.count)")
            let places = miArray2.count > 0 ? miArray2 : nil
            DataStore.shared.processNewItems(places)
        }
    }
    
//    private func displaySearchError(_ error: Error?) {
//        if let error = error as NSError?, let errorString = error.userInfo[NSLocalizedDescriptionKey] as? String {
//            let alertController = UIAlertController(title: "Could not find any places.", message: errorString, preferredStyle: .alert)
//            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//            if let delegate = delegate as? ViewController {
//                 delegate.present(alertController, animated: true, completion: nil)
//            }
//        }
//    }
}
