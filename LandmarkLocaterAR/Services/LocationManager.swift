//
//  LocationManager.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 1/15/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationManagerDelegate: class {
    func didUpdateCurrentLocation()
    func statusUpdate(message: String)
    func headingUpdate(message: String)
}
protocol RealTimeUpdateDelegate: class {
    func realTimeLocationUpdate()
}

class LocationManager: NSObject {
    
    static let shared = LocationManager()
    private let lm = CLLocationManager()
    private var APILocation: CLLocation?
    weak var delegate: LocationManagerDelegate?
    weak var realTimeDelegate: RealTimeUpdateDelegate?
    
    private let internalQueue = DispatchQueue(label: "com.brucefraser.landmarklocater.locationqueue", attributes: .concurrent)
    
    private var _accuracyAttempts = 9
    var accuracyAttempts: Int {
        get { return internalQueue.sync { _accuracyAttempts } }
        set (newState) { internalQueue.async(flags: .barrier) { self._accuracyAttempts = newState } }
    }
    
    private var _doAPICall = true
    var doAPICall: Bool {
        get { return internalQueue.sync { _doAPICall } }
        set (newState) { internalQueue.async(flags: .barrier) { self._doAPICall = newState } }
    }
    
    private var _doHeadingCall = false // get rid of this ????????????????
    var doHeadingCall: Bool {
        get { return internalQueue.sync { _doHeadingCall } }
        set (newState) { internalQueue.async(flags: .barrier) { self._doHeadingCall = newState } }
    }
    
    private var _currentLocation: CLLocation?
    var currentLocation: CLLocation? {
        get { return internalQueue.sync { _currentLocation } }
        set (newState) {
            internalQueue.async(flags: .barrier) { self._currentLocation = newState }
            realTimeDelegate?.realTimeLocationUpdate()
        }
    }
    
//    private var _realTimeLocation: CLLocation?
//    var realTimeLocation: CLLocation? {
//        get { return internalQueue.sync { _realTimeLocation } }
//        set (newState) { internalQueue.async(flags: .barrier) { self._realTimeLocation = newState } }
//    }
    
    private override init() {
        super.init()
        print("BGF --- location manager initialized")
        lm.desiredAccuracy = kCLLocationAccuracyBest; lm.distanceFilter = 5.0; lm.headingFilter = 10.0
        //lm.startUpdatingHeading()
        lm.delegate = self
    }
    
    func requestLocationUpdates() {
        guard CLLocationManager.locationServicesEnabled() else {
            displayLocationServicesDisabledAlert()
            return
        }
        let status = CLLocationManager.authorizationStatus()
        guard status != .denied else {
            displayLocationServicesDeniedAlert()
            return
        }
        lm.requestWhenInUseAuthorization()
        currentLocation = nil
        lm.startUpdatingLocation()
        lm.startUpdatingHeading()
        print("BGF --- start updating location")
    }
    
    func stopUpdating() {
        lm.startUpdatingHeading()
        lm.stopUpdatingLocation()
        print("BGF --- stop updating location")
    }
    
    func getLiveLocation() -> CLLocation? {
        lm.requestLocation()
        return lm.location
    }
    
    private func displayLocationServicesDisabledAlert() {
        let message = NSLocalizedString("LOCATION_SERVICES_DISABLED", comment: "Location services are disabled")
        let alertController = UIAlertController(title: NSLocalizedString("LOCATION_SERVICES_ALERT_TITLE", comment: "Location services alert title"), message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("BUTTON_OK", comment: "OK alert button"), style: .default, handler: nil))
        displayAlert(alertController)
    }
    
    private func displayLocationServicesErrorAlert(error: Error) {
        let message = "Location manager encountered error: \(error.localizedDescription). Is phone in airplane mode?"
        let alertController = UIAlertController(title: "Location Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("BUTTON_OK", comment: "OK alert button"), style: .default, handler: nil))
        displayAlert(alertController)
    }
    
    private func displayLocationServicesDeniedAlert() {
        let message = NSLocalizedString("LOCATION_SERVICES_DENIED", comment: "Location services are denied")
        let alertController = UIAlertController(title: NSLocalizedString("LOCATION_SERVICES_ALERT_TITLE", comment: "Location services alert title"), message: message, preferredStyle: .alert)
        let settingsButtonTitle = NSLocalizedString("BUTTON_SETTINGS", comment: "Settings alert button")
        let openSettingsAction = UIAlertAction(title: settingsButtonTitle, style: .default) { (_) in
            if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
            }
        }
        let cancelButtonTitle = NSLocalizedString("BUTTON_CANCEL", comment: "Location denied cancel button")
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(openSettingsAction)
        displayAlert(alertController)
    }
    
    var displayAlert = false
    var aController: UIAlertController? = nil
    
    private func displayAlert(_ controller: UIAlertController) {
        if let vc = UIApplication.shared.keyWindow?.rootViewController as? ViewController {
            vc.sceneView.session.pause()
            vc.present(controller, animated: true, completion: nil)
        } else {
            displayAlert = true
            aController = controller
        }
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let l = locations.last {
            print("BGF --- update locations called \(l) alt: \(l.altitude)")
        }
        let accuracyMax = UIDevice.current.userInterfaceIdiom != .phone ? 25.0: 10.0
        guard let location = locations.last else { return }
        if location.horizontalAccuracy > accuracyMax, accuracyAttempts > 0 {
            accuracyAttempts -= 1
            if currentLocation == nil {
                let mess = "Obtaining location accuracy: \(Int(location.horizontalAccuracy)) meters - stand by ..."
                delegate?.statusUpdate(message: mess)
            }
            return
        } else {
            accuracyAttempts = 8
        }
//        realTimeLocation = location
//        realTimeDelegate?.realTimeLocationUpdate()
        if let APILocationTemp = APILocation {
            if APILocationTemp.distance(from: location) > 1000 {
                APILocation = location; doAPICall = true    // be sure to flip this flag off after API call <-----------------------
                print("BGF --- api location distance exceeded")
            }
        } else {
            APILocation = location; doAPICall = true;
        }
        if currentLocation == nil || currentLocation!.distance(from: location) > 50 {
            currentLocation = location; delegate?.statusUpdate(message: "")
            print("BGF --- current location changed")
            delegate?.didUpdateCurrentLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("BGF --- location manager did fail")
        stopUpdating()
        displayLocationServicesErrorAlert(error: error)
    }
    
    func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool {
        return false
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
//        print("BGF --- updated heading \(newHeading)")
        if newHeading.headingAccuracy > 35 {
            let message = "Compass accuracy > \(Int(newHeading.headingAccuracy)) deg. Cars can disrupt accuracy or compass may need resetting."
            delegate?.headingUpdate(message: message)
        } else {
            delegate?.headingUpdate(message: "")
        }
        
    }
}

