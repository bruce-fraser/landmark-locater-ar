//
//  EditPlaceVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/19/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import MapKit

class EditPlaceVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var iconPicker: UIPickerView!
    @IBOutlet weak var spotImageView: UIImageView!
    @IBOutlet weak var showSignSwitch: UISwitch!
    @IBOutlet weak var saveButton: UIButton!
    
    var bgfPlaceItem: BGFPlaceItem!
    var isReceivedSpotController = false
    lazy var mainVC: ViewController = { return (UIApplication.shared.keyWindow?.rootViewController as? ViewController)!}()
    var sortedIconTypes = BGFPlaceItem.IconType.allCases
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameField.delegate = self
        title = "Edit"
        // Do any additional setup after loading the view.
        iconPicker.dataSource = self; iconPicker.delegate = self
        print("BGF --- edit view did load")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
         nameField.text = bgfPlaceItem.mapItem.name ?? "<empty>"
        showSignSwitch.isOn = bgfPlaceItem.showSign
        loadMapView() // create link to view in maps !!!!!!!!!!!!!
        sortedIconTypes = BGFPlaceItem.IconType.allCases.filter { Set(FREE_MODELS).contains($0.rawValue) || MySpotsProducts.store.isPurchased("com.brucefraser.LandmarkLocaterAR.iap." + $0.rawValue) }.sorted(by: <)
        let remainingIconTypes = BGFPlaceItem.IconType.allCases.filter { !Set(FREE_MODELS).contains($0.rawValue) && !MySpotsProducts.store.isPurchased("com.brucefraser.LandmarkLocaterAR.iap." + $0.rawValue) }.sorted(by: <)
        sortedIconTypes.append(contentsOf: remainingIconTypes)
        iconPicker.reloadAllComponents()
        if let r = sortedIconTypes.firstIndex(of: bgfPlaceItem.iconType) {
            iconPicker.selectRow(r, inComponent: 0, animated: false)
        }
        print("BGF --- edit view will appear")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.isNavigationBarHidden = false
        preferredContentSize = CGSize(width: 300, height: 310)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortedIconTypes.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return pickerView.bounds.height / 3
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let iView = UIImageView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.height / 3, height: pickerView.bounds.height / 3))
        let iName = sortedIconTypes[row].rawValue
        iView.image = UIImage(named: iName + "Image")
        if !Set(FREE_MODELS).contains(sortedIconTypes[row].rawValue) && !MySpotsProducts.store.isPurchased("com.brucefraser.LandmarkLocaterAR.iap." + iName) {
            let buyLabel = UILabel(frame: iView.bounds); buyLabel.textAlignment = .center
            buyLabel.adjustsFontSizeToFitWidth = true; buyLabel.text = "Buy"; buyLabel.textColor = UIColor.red
            iView.addSubview(buyLabel)
        }
        return iView
    }
    
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        var placeChanged = false
        if nameField.text != bgfPlaceItem.mapItem.name { bgfPlaceItem.mapItem.name = nameField.text; placeChanged = true }
        let idx = iconPicker.selectedRow(inComponent: 0)
        
        // Handle icons not yet bought
        let iconName = sortedIconTypes[idx].rawValue
        if !Set(FREE_MODELS).contains(iconName) && !MySpotsProducts.store.isPurchased("com.brucefraser.LandmarkLocaterAR.iap." + iconName) {
            displayBuyAlert()
            return
        }
        
        if sortedIconTypes[idx] != bgfPlaceItem.iconType {
            bgfPlaceItem.iconType = sortedIconTypes[idx]
            placeChanged = true
        }
        if showSignSwitch.isOn != bgfPlaceItem.showSign {
            bgfPlaceItem.showSign = !bgfPlaceItem.showSign; placeChanged = true
        }
        if placeChanged {
            DataStore.shared.customSerialQueue.async {
                print("BGF --- what??????")
                self.mainVC.reloadNode(for: self.bgfPlaceItem)
                if let sString = sender as? String, sString == "send" { return }
                DispatchQueue.main.async {
                    self.navigationController?.dismiss(animated: false, completion: nil)
                }
            }
        } else {
            if let sString = sender as? String, sString == "send" { return }
            navigationController?.popViewController(animated: true)
        }
        print("BGF --- end of save button tapped")
    }
    
    @IBAction func sendButtonTapped(_ sender: Any) {
        print("BGF --- send button tapped")
        saveButtonTapped("send")
        DispatchQueue.global(qos: .userInitiated).async {
            var err: Errors?
            print("BGF --- send spot from edit screen")
            let bgfJI = BGFJSONItem(bgfPlaceItem: self.bgfPlaceItem)
            if let data = try? JSONEncoder().encode(bgfJI), let docs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let path = docs.appendingPathComponent("/sharespot.mspt")
                do {
                    try data.write(to: path, options: .atomicWrite)
                    let intro = "I sent you a spot.  Click to open in My Spots AR (only on iPhone here \(APP_STORE_LINK).  Then scroll to 'Copy to My Spots AR'."
                    let items: [Any] = [intro, path]
                    DispatchQueue.main.async {
                        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                        ac.excludedActivityTypes = [UIActivity.ActivityType.message]
                        self.mainVC.present(ac, animated: true)
                    }
                } catch {
                    err = Errors.noWriteObjectToFile
                }
            } else {
                err = Errors.noPrepareObject
            }
            if let e = err {
                DispatchQueue.main.async {
                    self.mainVC.errorAlert(err: e)
                }
            }
        }
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        let confirmationAlert = UIAlertController(title: "Confirm", message: "Are you sure you want to delete this?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { [weak self] action in
           print("BGF --- delete confirmed")
            if let weakSelf = self {
                weakSelf.bgfPlaceItem.delete = true
                weakSelf.mainVC.reloadNode(for: weakSelf.bgfPlaceItem)
                weakSelf.navigationController?.popViewController(animated: true)
            }
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { action in
            print("BGF --- delete cancelled")
        }
        confirmationAlert.addAction(ok); confirmationAlert.addAction(cancel)
        present(confirmationAlert, animated: true, completion: nil)
    }
    
    func loadMapView() {
        let mapSnapshotOptions = MKMapSnapshotter.Options()
        let coor = bgfPlaceItem.mapItem.placemark.coordinate
        let region = MKCoordinateRegion(center: coor, latitudinalMeters: 2000, longitudinalMeters: 2000)
        mapSnapshotOptions.region = region
        mapSnapshotOptions.scale = UIScreen.main.scale
        mapSnapshotOptions.showsPointsOfInterest = true
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.size = CGSize(width: 150, height: 150)
        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
        snapShotter.start { [weak self] (sn, error) in
            if let image = sn?.image {
                self?.spotImageView.image = image
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func displayBuyAlert() {
        guard let vc = UIApplication.shared.keyWindow?.rootViewController as? ViewController else { return }
        let bAlert = UIAlertController(title: "Buy icon?", message: "Do you want to buy this icon?", preferredStyle: .alert)
        let noAction = UIAlertAction(title: "No", style: .cancel) { [weak self] (action) in
            self?.iconPicker.selectRow(0, inComponent: 0, animated: true)
        }
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            vc.dismiss(animated: false); vc.performSegue(withIdentifier: SegueIdentifiers.menuSegue.rawValue, sender: "store")
        }
        bAlert.addAction(noAction); bAlert.addAction(yesAction)
        present(bAlert, animated: true, completion: nil)
    }
}
