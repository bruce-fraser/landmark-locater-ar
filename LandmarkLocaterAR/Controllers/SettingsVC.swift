//
//  SettingsVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 3/11/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit

class SettingsVC: UITableViewController {

    lazy var mainVC: ViewController = { return (UIApplication.shared.keyWindow?.rootViewController as? ViewController)!}()
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var generalPlacesSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Settings"
        versionLabel.text = "\(APP_VERSION)"
        generalPlacesSwitch.isOn = DataStore.shared.showGeneralPlacesFlag
    }

    override func viewWillLayoutSubviews() {
         super.viewWillLayoutSubviews()
        navigationController?.isNavigationBarHidden = false
        preferredContentSize = CGSize(width: 300, height: 300)
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 2 && indexPath.row == 0 {
            (UIApplication.shared.keyWindow!.rootViewController as! ViewController).sendEmail()
        } else if indexPath.section == 2 && indexPath.row == 1 {
            let weakMainVC = mainVC
            navigationController?.dismiss(animated: false, completion: {
                let item = "Checkout this augmented reality app on iPhone!  \(APP_STORE_LINK)"
                let ac = UIActivityViewController(activityItems: [item], applicationActivities: nil)
                weakMainVC.present(ac, animated: true)
            })
        } else if indexPath.section == 2 && indexPath.row == 2 {
            if let url = URL(string: "\(WEB_SITE_URL)") {
                UIApplication.shared.open(url, options: [:])
            }
        } else if indexPath.section == 2 && indexPath.row == 3 {
            if let url = URL(string: APP_STORE_REVIEW_LINK) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
  
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return 88
        } else {
            return 44
        }
    }
    
    @IBAction func showSwitchChanged(_ sender: UISwitch) {
        let weakVC = mainVC
        DataStore.shared.showGeneralPlacesFlag = sender.isOn
        DataStore.shared.customSerialQueue.async {
            weakVC.performRenderPlaces()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
