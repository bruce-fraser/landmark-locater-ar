//
//  SearchTermsVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 3/13/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit

class SearchTermsVC: UITableViewController {

    var sTerms: [String]!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sTerms = DataStore.shared.searchTermsArray
    }

    // MARK: - View Controller Life Cycle
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.isNavigationBarHidden = false
        preferredContentSize = CGSize(width: 310, height: 310)
        tableView.backgroundColor = UIColor(named: "boneColor")!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if sTerms != DataStore.shared.searchTermsArray {
            print("BGF --- search arrays not equal")
            let alert = UIAlertController(title: "Save Changes?", message: "Would you like to save these changes?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
                DataStore.shared.searchTermsArray = self.sTerms
                if let vc = UIApplication.shared.keyWindow?.rootViewController as? ViewController {
                    vc.performAPICall()
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            if let vc = UIApplication.shared.keyWindow?.rootViewController {
                vc.dismiss(animated: false, completion: nil)
                vc.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Add Search Term", message: "Type your new search term.", preferredStyle: .alert)
        alert.addTextField { (tf) in
            tf.placeholder = "pizza restaurant"
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] (action) in
            if let tField = alert.textFields!.first {
                if let t = tField.text, t.count > 1 {
                    self?.sTerms.append(t); self?.tableView.reloadData()
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sTerms.count == 0 ? 1: sTerms.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard sTerms.count > 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "noResultsCell2")!
            navigationItem.rightBarButtonItems = [addButton]
            cell.backgroundColor = UIColor.clear
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchTermCell", for: indexPath)
        let term = sTerms[indexPath.row]
        cell.textLabel?.text = term
        cell.backgroundColor = UIColor.clear
        cell.backgroundView = CellBackgroundView1()
        if let items = navigationItem.rightBarButtonItems, items.count < 2 {
            navigationItem.rightBarButtonItems?.append(editButtonItem)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if sTerms.count > 0 {
             return true
        } else {
            return false
        }
    }

    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(style: .destructive, title: "Delete") { (action, ip) in
            self.sTerms.remove(at: ip.row)
            self.tableView.reloadData()
        }
        return [deleteButton]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
