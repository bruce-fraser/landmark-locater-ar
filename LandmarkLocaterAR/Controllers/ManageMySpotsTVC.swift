//
//  ManageMySpotsTVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/18/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import StoreKit

class ManageMySpotsTVC: UITableViewController {

    private var bgfPlaceItems: [BGFPlaceItem] = []
    var receivedSpotsController = false
    var bgfItem: BGFPlaceItem?
    
    // MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if receivedSpotsController {
            title = "Received Spots"
        } else {
            title = "My Spots"
        }
        handleStoreReview()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.isNavigationBarHidden = false
        preferredContentSize = CGSize(width: 310, height: 310)
        tableView.backgroundColor = UIColor(named: "boneColor")!
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let placesTemp = DataStore.shared.bgfPlaceItems {
            if receivedSpotsController {
                bgfPlaceItems = placesTemp.filter({ $0.loadedAs == .receivedSpot && $0.delete == false }).sorted(by: { $0.createdAt.compare($1.createdAt) == .orderedDescending })
            } else {
                bgfPlaceItems = placesTemp.filter({ $0.loadedAs == .mySpot && $0.delete == false }).sorted(by: { $0.createdAt.compare($1.createdAt) == .orderedDescending })
            }
        }
        if bgfPlaceItems.count > 0 {
            self.navigationItem.rightBarButtonItem = self.editButtonItem
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
        tableView.reloadData()
        if bgfItem != nil {
            performSegue(withIdentifier: SegueIdentifiers.editPlaceSegue.rawValue, sender: nil)
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return bgfPlaceItems.count == 0 ? 1: bgfPlaceItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard bgfPlaceItems.count > 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "noResultsCell")!
            cell.backgroundColor = UIColor.clear
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "manageSpotCell", for: indexPath)
        let place = bgfPlaceItems[indexPath.row]
        cell.textLabel?.text = place.mapItem.name
        cell.detailTextLabel?.text = place.createdAt.description(with: Locale(identifier: "en_US"))
        cell.backgroundColor = UIColor.clear
        cell.backgroundView = CellBackgroundView1()
        
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool { return true }

//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            bgfPlaceItems[indexPath.row].delete = true
//            if let mainVC = navigationController?.parent as? ViewController {
//                mainVC.reloadAnchor(for: bgfPlaceItems[indexPath.row])
//            }
//            tableView.deleteRows(at: [indexPath], with: .fade)
//        }
//    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(style: .destructive, title: "Delete") { [weak self] (action, ip) in
            print("BGF --- delete pushed for row \(ip.row)")
            if let weakSelf = self, let mainVC = UIApplication.shared.keyWindow?.rootViewController as? ViewController {
                print("BGF --- thissssssssssssssssssssssssssssssssssssss?")
                weakSelf.bgfPlaceItems[ip.row].delete = true
                mainVC.reloadNode(for: weakSelf.bgfPlaceItems[ip.row])
                weakSelf.bgfPlaceItems.remove(at: ip.row)
                weakSelf.tableView.reloadData()
            }
        }
        let editButton = UITableViewRowAction(style: .normal, title: "Edit") { [weak self] (action, ip) in
            print("BGF --- edit pushed for row \(ip.row)")
            if let weakSelf = self {
                weakSelf.performSegue(withIdentifier: "editPlaceSegue", sender: ip)
            }
        }
        editButton.backgroundColor = UIColor(named: "iguanaGreenColor")!
        return [deleteButton, editButton]
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.editPlaceSegue.rawValue, let dest = segue.destination as? EditPlaceVC {
            if bgfItem != nil {
                dest.bgfPlaceItem = bgfItem!
                bgfItem = nil
            } else {
                let iPath = tableView.indexPathForSelectedRow ?? sender as? IndexPath
                if let idx = iPath {
                    dest.bgfPlaceItem = bgfPlaceItems[idx.row]
                }
            }
            dest.isReceivedSpotController = receivedSpotsController
        }
    }
    
    func handleStoreReview () {
        var count = UserDefaults.standard.integer(forKey: UserDefaultsKeys.processComletedCountKey.rawValue)
        count += 1
        UserDefaults.standard.set(count, forKey: UserDefaultsKeys.processComletedCountKey.rawValue)
        print("BGF --- process completed: \(count) times")
        let infoDictionaryKey = kCFBundleVersionKey as String
        guard let currentVersion = Bundle.main.object(forInfoDictionaryKey: infoDictionaryKey) as? String else { return }
        let lastVersionPromptedForReview = UserDefaults.standard.string(forKey: UserDefaultsKeys.lastVersioPromptedForReviewKey.rawValue)
        if count >= 30 && currentVersion != lastVersionPromptedForReview {
            let twoSecondsFromNow = DispatchTime.now() + 2.0
            DispatchQueue.main.asyncAfter(deadline: twoSecondsFromNow) { [navigationController] in
                if navigationController?.topViewController is ManageMySpotsTVC {
                    SKStoreReviewController.requestReview()
                    UserDefaults.standard.set(currentVersion, forKey: UserDefaultsKeys.lastVersioPromptedForReviewKey.rawValue)
                }
            }
        }
    }
}
