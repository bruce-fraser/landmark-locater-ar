//
//  MapViewVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/11/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import MapKit

class MapViewVC: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    var overlayAdded: MKOverlay?
    var satelliteModeOn: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationManager.shared.realTimeDelegate = self
        DataStore.shared.mapDisplayDelegate = self
        mapView.delegate = self
        mapView.showsCompass = false
        mapView.userTrackingMode = .followWithHeading
        mapView.isScrollEnabled = true
        mapView.isRotateEnabled = false
        mapView.register(BGFPlaceView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
    }
    
    func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {
        print("BGF --- map view failed to load")
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
//        mapView.userTrackingMode = .followWithHeading
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is ARRangeOverlay {
            print("BGF --- overlay view called .........................................")
            return ARRangeOverlayView(overlay: overlay, overlayImage: UIImage(named: "map-overlay")!)
        }
        return MKOverlayRenderer()
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotation = view.annotation as? BGFPlaceItem else { return }
        annotation.mapItem.openInMaps()
    }
    
    @IBAction func satelliteMode(_ sender: Any) {
        satelliteModeOn = !satelliteModeOn
        mapView.mapType = satelliteModeOn ? .satellite : .standard
    }
    
    @IBAction func recenter(_ sender: Any) {
        let tm = mapView.userTrackingMode
        mapView.userTrackingMode = tm == .followWithHeading ? .none : .followWithHeading
        if let button = sender as? UIButton {
            let tColor = mapView.userTrackingMode == .followWithHeading ? UIColor.blue: UIColor.white
            button.setTitleColor(tColor, for: .normal)
        }
    }
}

extension MapViewVC: RealTimeUpdateDelegate {
    func realTimeLocationUpdate() { // really current location update every 50 meters
//        if let loc = LocationManager.shared.realTimeLocation {
//            let region = MKCoordinateRegion(center: loc.coordinate, latitudinalMeters: 300, longitudinalMeters: 300)
//            mapView.region = region
//        }
        
        if let cl = LocationManager.shared.currentLocation?.coordinate {
            if let mo = overlayAdded {
                mapView.removeOverlay(mo)
            }
            let overlay = ARRangeOverlay(cl)
            mapView.addOverlay(overlay)
            overlayAdded = overlay
            print("BGF --- added map overlay")
        }
    }
}

extension MapViewVC: MapDisplayDelegate {
    func didFetchPlaces(recenter: Bool) {
        print("BGF --- map vc process fetched places")
        if let pi = DataStore.shared.bgfPlaceItems {
            let pi2 = pi.filter { !($0.loadedAs == .generalPlace && $0.favorited == false)}.filter { $0.delete == false }
            DispatchQueue.main.async {
                self.mapView.removeAnnotations(self.mapView.annotations)
                self.mapView.addAnnotations(pi2)
            }
        }
        if  recenter, let loc = LocationManager.shared.currentLocation {
            let reg = MKCoordinateRegion(center: loc.coordinate, latitudinalMeters: 1500, longitudinalMeters: 1500)
            print("BGF --- map zoom out")
            DispatchQueue.main.async { [unowned self] in
                self.mapView.region = reg
            }
        }
    }
    func addedReceivedPlace(place: BGFPlaceItem) {
        print("BGF --- added received palce annotation")
        mapView.addAnnotation(place)
        let loc = place.mapItem.placemark.coordinate
        let region = MKCoordinateRegion(center: loc, latitudinalMeters: 800, longitudinalMeters: 800)
        mapView.region = region
        mapView.selectAnnotation(place, animated: false)
    }
}
