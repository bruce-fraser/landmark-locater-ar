//
//  storeTVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 3/30/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import StoreKit

class storeTVC: UITableViewController {

    var products = [SKProduct]()
    private lazy var priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Store"
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(requestAllProducts), for: .valueChanged)
        refreshControl?.beginRefreshing()
        requestAllProducts()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Restore", style: .plain, target: self, action: #selector(restoreTapped))
        NotificationCenter.default.addObserver(self, selector: #selector(handlePurchaseNotification(_:)), name: .purchaseNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.isNavigationBarHidden = false
        preferredContentSize = CGSize(width: 310, height: 310)
        tableView.backgroundColor = UIColor(named: "boneColor")!
    }
    
    @objc func requestAllProducts() {
        MySpotsProducts.store.requestProducts {
            [weak self] success, products in
            if success, let products = products {
                self?.products = products
                self?.tableView.reloadData()
            }
            self?.refreshControl?.endRefreshing()
        }
    }
    
    @objc func restoreTapped(_ sender: AnyObject) {
        MySpotsProducts.store.restorePurchases()
    }
    
    @objc func handlePurchaseNotification(_ notification: Notification) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return products.count > 0 ? products.count: 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard products.count > 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "storeNotAvailableCell")!
            cell.backgroundColor = UIColor.clear
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "storeCell", for: indexPath)
        let prod = products[indexPath.row]
        let lTitle = MySpotsProducts.store.isPurchased(prod.productIdentifier) ? "✅ " + prod.localizedTitle: prod.localizedTitle
        cell.textLabel?.text = lTitle
        priceFormatter.locale = prod.priceLocale
        cell.detailTextLabel?.text = priceFormatter.string(from: prod.price)
        if let iName = prod.productIdentifier.components(separatedBy: ".").last {
            cell.imageView?.image = UIImage(named: iName + "Image")
        }
        cell.backgroundColor = UIColor.clear
//        cell.backgroundView = CellBackgroundView1()
        
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let iPath = tableView.indexPathForSelectedRow, let vc = segue.destination as? storeDetailVC  else { return }
        let prod = products[iPath.row]
        vc.product = prod
    }
}
