//
//  SearchResultTableVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/7/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import MapKit

class SearchResultTableVC: UITableViewController {

    @IBOutlet private var insertAllButton: UIBarButtonItem!
    @IBOutlet private var clearSearchButton: UIBarButtonItem!
    
    private var places: [MKMapItem]? {
        didSet {
            tableView.reloadData()
            insertAllButton.isEnabled = places != nil
        }
    }
    private var suggestionController: SuggestionsTableTableViewController!
    private var searchController: UISearchController!
    private var localSearch: MKLocalSearch? {
        willSet {
            places = nil
            localSearch?.cancel()
        }
    }
    private var boundingRegion: MKCoordinateRegion?
    
    override func awakeFromNib() {
        super.awakeFromNib(); print("BGF --- search awake from nib")
        suggestionController = SuggestionsTableTableViewController()
        suggestionController.tableView.delegate = self
        
        searchController = UISearchController(searchResultsController: suggestionController)
        searchController.searchResultsUpdater = suggestionController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad(); print("BGF --- search view did load")
        clearSearchButton.isEnabled = DataStore.shared.displayingSearchResults
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        tableView.backgroundColor = UIColor(named: "boneColor")!
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.configure()
        searchController.searchBar.delegate = self
        definesPresentationContext = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated); print("BGF --- search view did appear")
        guard let parentVC = popoverPresentationController?.delegate as? ViewController else { return }
        if let loc = parentVC.locationManager.currentLocation {
            let region = MKCoordinateRegion(center: loc.coordinate, latitudinalMeters: 900, longitudinalMeters: 900)
            suggestionController.searchCompleter.region = region
            boundingRegion = region
            tableView.reloadData()
        } // add view layout with preferred size ................................................
    }
    
    override func viewWillLayoutSubviews() {
        navigationController?.isNavigationBarHidden = false
        preferredContentSize = CGSize(width: 300, height: 310)
    }
    
    @IBAction func insertAllTapped() {
        print("BGF --- insert all tapped")
        if let mkItems = places {
            DataStore.shared.processNewItems(mkItems, of: .searchResult)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func clearSearchTapped() {
        print("BGF --- clear search tapped")
        DataStore.shared.processNewItems(nil, of: .searchResult)
        dismiss(animated: true, completion: nil)
    }
    
    private func search(for suggestedCompletion: MKLocalSearchCompletion) {
        let searchRequest = MKLocalSearch.Request(completion: suggestedCompletion)
        search(using: searchRequest)
    }
    
    private func search(for queryString: String?) {
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = queryString
        search(using: searchRequest)
    }
    
    private func search(using searchRequest: MKLocalSearch.Request) {
        // Confine the map search area to an area around the user's current location.
        if let region = boundingRegion {
            searchRequest.region = region
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        localSearch = MKLocalSearch(request: searchRequest)
        localSearch?.start { [weak self] (response, error) in
            guard error == nil else {
                self?.displaySearchError(error)
                return
            }
            self?.places = response?.mapItems
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    private func displaySearchError(_ error: Error?) {
        if let error = error as NSError?, let errorString = error.userInfo[NSLocalizedDescriptionKey] as? String {
            let alertController = UIAlertController(title: "Could not find any places.", message: errorString, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
}

extension SearchResultTableVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultCell", for: indexPath)
        if let mapItem = places?[indexPath.row] {
            cell.textLabel?.text = mapItem.name
            cell.detailTextLabel?.text = mapItem.placemark.formattedAddress
        }
        cell.backgroundColor = .clear
        cell.backgroundView = CellBackgroundView1()
        return cell
    }
}

extension SearchResultTableVC {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView == suggestionController.tableView, let suggestion = suggestionController.completerResults?[indexPath.row] {
            searchController.isActive = false
            searchController.searchBar.text = suggestion.title
            search(for: suggestion)
        }
        if tableView == self.tableView {
            if let mkItem = places?[indexPath.row] {
                DataStore.shared.processNewItems([mkItem], of: .searchResult)
                dismiss(animated: true, completion: nil)
            }
        }
    }
}

extension SearchResultTableVC: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        dismiss(animated: true, completion: nil)
        // The user tapped search on the `UISearchBar` or on the keyboard. Since they didn't
        // select a row with a suggested completion, run the search with the query text in the search field.
        search(for: searchBar.text)
    }
}
