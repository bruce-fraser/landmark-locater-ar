//
//  ViewController.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 12/24/18.
//  Copyright © 2018 Bruce Fraser. All rights reserved.
//

import UIKit; import SceneKit; import ARKit; import CoreLocation; import MapKit; import MessageUI

class ViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var vEView: UIVisualEffectView!
    @IBOutlet weak var statusLabel: UILabel!
    
    let locationManager = LocationManager.shared
    var cameraPositionNode = SCNNode()
    var renderPlaces: Bool = false
    let formatter = MeasurementFormatter()
    private lazy var mapVC: MapViewVC = { return children.first as! MapViewVC }()
    
    private let internalQueue = DispatchQueue(label: "com.brucefraser.landmarklocater.vcqueue", attributes: .concurrent)
    
    private var _renderedPlacesArray = [BGFPlaceItem]()
    var renderedPlacesArray: [BGFPlaceItem] {
        get { return internalQueue.sync { _renderedPlacesArray } }
        set (newState) { internalQueue.async(flags: .barrier) { self._renderedPlacesArray = newState } }
    }
    
    private var _groupedNodes: [(SCNNode, SCNVector3)]? = nil
    var groupedNodes: [(SCNNode, SCNVector3)]? {
        get { return internalQueue.sync { _groupedNodes } }
        set (newState) { internalQueue.async(flags: .barrier) { self._groupedNodes = newState } }
    }
    
    private var _statusMessagesArray: [String] = ["", "", ""]
    var statusMessagesArray: [String] {
        get { return internalQueue.sync { _statusMessagesArray } }
        set (newState) { internalQueue.async(flags: .barrier) { self._statusMessagesArray = newState }
            let combinedMessage = statusMessagesArray.reduce("", {$0 == "" ? $1: $1 == "" ? $0: $0 + "\n\n" + $1})
            DispatchQueue.main.async {
                self.statusLabel.text = combinedMessage; self.vEView.isHidden = combinedMessage.count < 1
            }
        }
    }
    
    private var _previousSelectedNode: SCNNode? = nil
    var previousSelectedNode: SCNNode? {
        get { return internalQueue.sync { _previousSelectedNode } }
        set (newState) { internalQueue.async(flags: .barrier) { self._previousSelectedNode = newState } }
    }
            
    private var _selectedNode: SCNNode? = nil
    var selectedNode: SCNNode? {
        get { return internalQueue.sync { _selectedNode } }
        set (newState) { previousSelectedNode = _selectedNode; internalQueue.async(flags: .barrier) { self._selectedNode = newState }
    
            if let sn = selectedNode, sn !== previousSelectedNode, let cl = locationManager.getLiveLocation(), let placeObject = getPlaceObject(for: sn) {
                sn.geometry?.firstMaterial?.emission.contents = UIColor.lightGray
                sn.enumerateChildNodes { (n, _) in
                    n.geometry?.firstMaterial?.emission.contents = UIColor.lightGray
                }
                SCNTransaction.begin(); SCNTransaction.animationDuration = 0.5
                SCNTransaction.animationTimingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
                sn.scale *= 1.5; sn.position.y *= 1.5
                SCNTransaction.commit()
                
                let dist = placeObject.mapItem.clLocation.distance(from: cl)
                let distMetersOrKilometers = Measurement(value: dist, unit: UnitLength.meters)
                let title = placeObject.favorited ? "♥︎": "♡"
//                mapVC.mapView.removeAnnotations(mapVC.mapView.annotations)
                if !(placeObject.loadedAs == .generalPlace && placeObject.favorited == false) {
                    mapVC.mapView.selectAnnotation(placeObject, animated: false)
                }
                DispatchQueue.main.async {
                    self.placeNameLabel.text = placeObject.mapItem.name
                    self.placeInfoDistance.text = self.formatter.string(from: distMetersOrKilometers)
                    if placeObject.loadedAs == .generalPlace || placeObject.loadedAs == .searchResult {
                        self.favoriteButton.setTitle(title, for: UIControl.State.normal)
                        self.favoriteButton.setTitleColor(UIColor.red, for: .normal)
                        self.favoriteButton.isHidden = false
                    } else {
                        self.favoriteButton.setTitle("⚙︎", for: .normal)
                        self.favoriteButton.setTitleColor(UIColor(named: "sandyBrownColor"), for: .normal)
                        self.favoriteButton.isHidden = false
                    }
                    UIView.transition(with: self.placeInfoView, duration: 0.5, options: .transitionCurlDown, animations: {
                        self.placeInfoView.isHidden = false
                    }, completion: nil)
                }
            }
            if let ov = previousSelectedNode, ov !== selectedNode {
                ov.geometry?.firstMaterial?.emission.contents = UIColor.black
                ov.enumerateChildNodes { (n, _) in
                    n.geometry?.firstMaterial?.emission.contents = UIColor.black
                }
                SCNTransaction.begin(); SCNTransaction.animationDuration = 0.5
                SCNTransaction.animationTimingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
                ov.scale *= 0.67; ov.position.y *= 0.67
                SCNTransaction.commit()
            }
            if selectedNode == nil {
                let sa = mapVC.mapView.selectedAnnotations
                for a in sa {
                    mapVC.mapView.deselectAnnotation(a, animated: true)
                }
                DispatchQueue.main.async {
                    UIView.transition(with: self.placeInfoView, duration: 0.5, options: .transitionCurlUp, animations: {
                        self.placeInfoView.alpha = 0.0
                    }, completion: { (_) in
                        self.placeInfoView.isHidden = true
                        self.placeInfoView.alpha = 1.0
                    })
                }
            }
        }
    }
    // Place Information View
    @IBOutlet weak var placeInfoView: UIVisualEffectView!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeInfoDistance: UILabel!
    
    // MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad(); print("BGF --- view did load")
        locationManager.delegate = self
        formatter.unitOptions = .naturalScale; formatter.unitStyle = .long
        let n = NumberFormatter(); n.maximumFractionDigits = 1; formatter.numberFormatter = n
        sceneView.delegate = self
        sceneView.session.delegate = self
        sceneView.autoenablesDefaultLighting = true
        sceneView.showsStatistics = false; //sceneView.debugOptions = [.showWorldOrigin]
        sceneView.scene = SCNScene()
        cameraPositionNode.position = sceneView.pointOfView?.position ?? SCNVector3(0,0,0)
        sceneView.scene.rootNode.addChildNode(cameraPositionNode)
        DispatchQueue.global(qos: .userInteractive).async {
             self.createDirectionNodes()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated); print("BGF --- view will appear")
        locationManager.requestLocationUpdates()
        selectedNode = nil
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);   print("BGF --- view will disappear")
        sceneView.session.pause()
        locationManager.stopUpdating()
//        DataStore.shared.storeItems()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        dismiss(animated: true, completion: nil)
        if locationManager.displayAlert {
            locationManager.displayAlert = false
            sceneView.session.pause(); statusMessagesArray[0] = "Location services not available."
            if let aC = locationManager.aController {
                locationManager.aController = nil
                present(aC, animated: true)
            }
        }
    }
    
    // MARK: - ARSCNViewDelegate
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    }
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if let newPosition = sceneView.pointOfView?.position {
            cameraPositionNode.position = newPosition
        }
    }

    @IBAction func handleSceneTap(_ sender: UITapGestureRecognizer) {
        print("BGF --- handle tap.")
        let results = sceneView.hitTest(sender.location(in: sceneView), options: [SCNHitTestOption.searchMode: SCNHitTestSearchMode.all.rawValue]).compactMap { (r) -> SCNNode? in
            var pNode = r.node
            while true {
                if let name = pNode.name, name.hasPrefix("place") {
                    return pNode
                } else {
                    if let p = pNode.parent {
                        pNode = p
                    } else {
                        return nil
                    }
                }
            }
        }
        print("BGF --- node count: \(results.count)")
        let uniqueSet = Set(results); let results2 = Array(uniqueSet)
        print("BGF --- node count after set: \(results2.count)")
        if groupedNodes != nil { unloadGroup() }
        switch results2.count {
        case 0: selectedNode = nil;
        case 1: selectedNode = results2.first!;
        default: loadGroup(nodes: results2)
        }
    }
    
    func unloadGroup() {
        guard let group = groupedNodes else { return }
        DataStore.shared.customSerialQueue.async { // is this a retain cycle?
            SCNTransaction.begin(); SCNTransaction.animationDuration = 0.5
            SCNTransaction.animationTimingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            for (n, p) in group {
                n.position = p
            }
            SCNTransaction.commit()
            self.groupedNodes = nil
        }
    }
    
    func loadGroup(nodes: [SCNNode]) {
        let cameraPosition = sceneView.pointOfView?.position ?? SCNVector3(x: 0, y: 0, z: 0)
        DataStore.shared.customSerialQueue.async { // is this a retain cycle?
            var gNodes = [(SCNNode, SCNVector3)]()
            SCNTransaction.begin(); SCNTransaction.animationDuration = 0.5
            SCNTransaction.animationTimingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            for (i, n) in nodes.enumerated() {
                gNodes.append((n, n.position))
                let hDist = n.position.horizontalDistance(to: cameraPosition)
                n.position.y += Float((i + 2) * 10) * hDist / 200
            }
            SCNTransaction.commit()
            self.groupedNodes = gNodes
        }
    }
    
    // MARK: - AR Session
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        statusMessagesArray[1] = "AR session failed."
        print("BGF --- Session did fail")
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        statusMessagesArray[1] = "AR session interrupted."
        print("BGF --- session interrupted")
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        statusMessagesArray[1] = "AR interruption ended."
        print("BGF --- session interruption ended")
    }
    
    func sessionShouldAttemptRelocalization(_ session: ARSession) -> Bool {
        return false
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .normal:
            statusMessagesArray[1] = ""
            print("BGF --- normal tracking ... \(camera.trackingState)")
        case .notAvailable:
            statusMessagesArray[1] = "Tracking Unavailable."
        case .limited(.excessiveMotion):
            statusMessagesArray[1] = "Tracking limited - move device more slowly."
        case .limited(.insufficientFeatures):
            statusMessagesArray[1] = "Tracking limited - poor detail or lighting."
        case .limited(.relocalizing):
            statusMessagesArray[1] = "Resuming session — move to where you were when the session was interrupted."
        case .limited(.initializing):
            statusMessagesArray[1] = "Initializing AR session."
        case .limited(_):
            statusMessagesArray[1] = ""
        }
    }
    
//    func session(_ session: ARSession, didUpdate frame: ARFrame) {
//        switch frame.worldMappingStatus {
//        case .extending, .mapped :
//            print("BGF --- world mapping ... \(frame.worldMappingStatus) ..................................")
//        default:
//            break
//        }
//    }
    // MARK: - Main Logic
    @IBAction func restartPushed(_ sender: Any) {
        sceneView.session.pause()
        locationManager.stopUpdating()
        locationManager.requestLocationUpdates()
    }
    
    private func getPlaceObject(for node: SCNNode) -> BGFPlaceItem? {
        if let name = node.name, name.hasPrefix("place") {
            let startI = name.index(name.startIndex, offsetBy: 5)
            let subS = name[startI..<name.endIndex];
            guard let arrayI = Int(subS) else { return nil }
            let placeObject = renderedPlacesArray[arrayI]
            return placeObject
        }
        return nil
    }
    
    func reloadNode(for placeItem: BGFPlaceItem) {
        if let idx = renderedPlacesArray.firstIndex(of: placeItem) {
            if let node = sceneView.scene.rootNode.childNodes.filter({ $0.name == "place\(idx)" }).first {
                DispatchQueue.global(qos: .userInteractive).sync {
                    self.selectedNode = nil
                }
                let nodePosition = node.position
                SCNTransaction.begin(); SCNTransaction.animationDuration = 0.8
                node.scale = SCNVector3(x: 0.01, y: 0.01, z: 0.01)
                SCNTransaction.completionBlock = { [weak self] in
                    node.removeFromParentNode()
                    if placeItem.delete == false, let cl = LocationManager.shared.currentLocation {
                        DataStore.shared.customSerialQueue.async {
                            self?.createPlaceNode(ind: idx, place: placeItem, cl: cl, position: nodePosition)
                            self?.mapVC.didFetchPlaces(recenter: false)
                        }
                    } else {
                          self?.mapVC.didFetchPlaces(recenter: false)
                    }
                }
                SCNTransaction.commit()
            }
        }
    }
    
    func displayPlane(_ pName: String, color: UIColor, textColor: UIColor = .white, iconType: BGFPlaceItem.IconType) -> SCNNode {
        let skScene = SKScene(size: CGSize(width: 800, height: 200)); skScene.backgroundColor = UIColor.clear
        let rectangle = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 800, height: 200), cornerRadius: 20)
        rectangle.fillColor = color; rectangle.strokeColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1); rectangle.lineWidth = 5; rectangle.alpha = 1.0
        var pName = pName; if pName.count > 13 { pName = pName.prefix(12) + "..." }
        let labelNode = SKLabelNode(text: pName)
        labelNode.fontSize = 105; labelNode.fontName = "Futura"; labelNode.fontColor = textColor
        labelNode.yScale = -1; labelNode.position = CGPoint(x:400,y:132);
        skScene.addChild(rectangle); skScene.addChild(labelNode)
        let plane = SCNPlane(width: 8, height: 2)
        let material = SCNMaterial(); material.isDoubleSided = false
        material.diffuse.contents = skScene; plane.materials = [material]
        let node = SCNNode(geometry: plane)
        let yValue = iconType.yValue
        node.position = SCNVector3(0, yValue, 0); node.constraints = [SCNBillboardConstraint()]
        node.name = "sign"
        return node
    }
    
    func createDirectionNodes() {
        [(d: "WEST", p: (x: Float(-1.0), z: Float(0.0)),  r: Float.pi/2),
         (d: "NORTH", p: (x: Float(0.0),  z: Float(-1.0)), r: Float(0.0)),
         (d: "EAST", p: (x: Float(1.0),  z: Float(0.0)),  r: -Float.pi/2),
         (d: "SOUTH", p: (x: Float(0.0), z: Float(1.0)),   r: Float.pi)  ].forEach { (arg: (d: String, p: (x: Float, z: Float), r: Float)) in
            let (d, p, r) = arg
            let dText = SCNText(string: d, extrusionDepth: 2)
            let dNode = SCNNode(geometry: dText)
            let (minVec, maxVec) = dNode.boundingBox
            dNode.pivot = SCNMatrix4MakeTranslation((maxVec.x - minVec.x) / 2 + minVec.x, (maxVec.y - minVec.y) / 2 + minVec.y, 0)
            dText.firstMaterial?.diffuse.contents = UIColor.green; dText.firstMaterial?.lightingModel = .physicallyBased
            let backPlane = SCNPlane(width: 60, height: 40); backPlane.cornerRadius = 10
            backPlane.firstMaterial?.diffuse.contents = UIColor.black.withAlphaComponent(0.9)
            let bPlaneNode = SCNNode(geometry: backPlane);
            bPlaneNode.position = SCNVector3(x: (maxVec.x - minVec.x) / 2 + minVec.x, y: 5.0, z: -4)
            dNode.addChildNode(bPlaneNode)
            dNode.scale = SCNVector3(0.007, 0.007, 0.007); dNode.position = SCNVector3(p.x, -0.3, p.z);
            dNode.eulerAngles.y = r;
            cameraPositionNode.addChildNode(dNode)
        }
    }
    
    func createSession() {
        sceneView.scene.rootNode.enumerateChildNodes { n, _ in
            if let name = n.name, name.hasPrefix("place") { n.removeFromParentNode() }
        }
        let configuration = ARWorldTrackingConfiguration(); print("BGF --- create session called")
        configuration.worldAlignment = .gravityAndHeading; //configuration.planeDetection = [.vertical]
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        selectedNode = nil ;renderedPlacesArray = [BGFPlaceItem]()
        if locationManager.doAPICall  {
            performAPICall()
        } else {
            DataStore.shared.customSerialQueue.async { [weak self] in
                self?.performRenderPlaces()
            }
        }
    }
    
    deinit { print("BGF --- deinit") }
    
    func performAPICall () {
        locationManager.doAPICall = false; print("BGF --- performAPICall")
        if let cl = locationManager.currentLocation {
            DataStore.shared.delegate = self
            PlacesService.service.search(coordinate: cl.coordinate)
        } else {
            print("BGF --- no currentLocation in performAPICall") // error message ...........
        }
    }
    
    func addMySpot(spot: BGFPlaceItem) {
        guard let currentLocation = locationManager.currentLocation else {
            let err = Errors.noLiveLocation
            errorAlert(err: err)
            return
        }
        let ind = renderedPlacesArray.count
        renderedPlacesArray.append(spot)
        if DataStore.shared.bgfPlaceItems != nil {
            DataStore.shared.bgfPlaceItems!.append(spot)
            print("BGF --- added mySpot to bgfPlaceItems")
        } else {
            DataStore.shared.bgfPlaceItems = [spot]
        }
        createPlaceNode(ind: ind, place: spot, cl: currentLocation)
        mapVC.didFetchPlaces(recenter: false)
        let msg = Errors.mySpotAdded
        errorAlert(err: msg)
    }
    
    func performRenderPlaces () {
        guard let currentLocation = locationManager.currentLocation else { return }
        selectedNode = nil
        sceneView.scene.rootNode.enumerateChildNodes { n, _ in
            if let name = n.name, name.hasPrefix("place") { n.removeFromParentNode() }
        }
        print("BGF --- current location \(currentLocation.coordinate), altitude: \(currentLocation.altitude)")
        if let nearbyPlaces =  DataStore.shared.bgfPlaceItems {
            let nearbyPlaces2 = nearbyPlaces.filter({ (placeItem) -> Bool in
                return placeItem.mapItem.clLocation.distance(from: currentLocation) < 750 && placeItem.delete == false
            })
            if DataStore.shared.showGeneralPlacesFlag {
                renderedPlacesArray = nearbyPlaces2
            } else {
                renderedPlacesArray = nearbyPlaces2.filter({ (placeItem) -> Bool in
                    return !(placeItem.loadedAs == .generalPlace && !placeItem.favorited)
                })
            }
            print("BGF --- rendered places array count: \(renderedPlacesArray.count)")
            for (index, place) in renderedPlacesArray.enumerated() {
                createPlaceNode(ind: index, place: place, cl: currentLocation)
            }
        } else {
            print("BGF --- no BGFPlaceItems")
            // display message ..................................................................
        }
    }
   
    func createPlaceNode(ind: Int, place: BGFPlaceItem, cl: CLLocation, position: SCNVector3? = nil) {
        let distance = Float(place.mapItem.clLocation.distance(from: cl))
        let anchorTransform = transformMatrix(matrix_identity_float4x4, cl, place.mapItem.clLocation, distance)
        
        var shape: SCNGeometry
        switch place.iconType {
        case .cube: shape = SCNBox(width: 1.0, height: 1.0, length: 1.0, chamferRadius: 0.1)
        default: shape = SCNSphere(radius: 0.8)
        }
        var renderColor = UIColor(named: "favoriteColor")!
        if !place.favorited { renderColor = place.loadedAs.color }
        shape.firstMaterial?.diffuse.contents = renderColor; shape.firstMaterial?.lightingModel = .physicallyBased
        var shapeNode: SCNNode
        if !DataStore.shared.availableIcons.contains(place.iconType.rawValue) {
            place.iconType = .sphere
        }
        switch place.iconType {
        case .sphere, .cube: shapeNode = SCNNode(geometry: shape)
        default: shapeNode = loadModel(place.iconType)
        }
        let shapeHeight = place.loadedAs == .mySpot || place.loadedAs == .receivedSpot ? place.altitude - cl.altitude : place.altitude + 15.0
        if place.showSign {
            let pName = place.mapItem.name ?? "unknown"
            shapeNode.addChildNode(displayPlane(pName, color: renderColor, iconType: place.iconType))
        }
        shapeNode.name = "place\(ind)";
       
        let dist = cl.distance(from: place.mapItem.clLocation)
        let scale = dist < 50 ? 1.0: dist * 0.018 + 1.0
        shapeNode.scale = SCNVector3(scale, scale, scale)
        let height = dist < 30 ? shapeHeight: dist * dist * 0.0005 + shapeHeight
        let placePosition = SCNVector3Make(anchorTransform.columns.3.x, Float(height), anchorTransform.columns.3.z)
        shapeNode.position = position ?? placePosition
        if dist < 25.0 && (place.loadedAs == .mySpot || place.loadedAs == .receivedSpot) {
            shapeNode.enumerateChildNodes { (n, _) in
                if let name = n.name, name == "sign" { n.scale = SCNVector3(0.3, 0.3, 0.3) }
            }
        }
        let shapeScale = shapeNode.scale
        switch place.iconType {
        case .smirk, .love, .dizzy, .happy, .bawling, .confused, .surprised, .worried, .grimace, .rage: let bc = SCNBillboardConstraint(); bc.freeAxes = .Y
            shapeNode.constraints = [bc]
        default: break
        }
        shapeNode.scale = SCNVector3(x: 0.02, y: 0.02, z: 0.02)
        sceneView.scene.rootNode.addChildNode(shapeNode)
        SCNTransaction.begin(); SCNTransaction.animationDuration = 0.5
        SCNTransaction.animationTimingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        shapeNode.scale = shapeScale
        SCNTransaction.commit()
        print("BGF --- addeded place nodes")
    }
    
    func loadModel(_ icon: BGFPlaceItem.IconType) -> SCNNode {
        let sceneURL = Bundle.main.url(forResource: icon.rawValue, withExtension: "scn", subdirectory: "art.scnassets")!
        let referenceNode = SCNReferenceNode(url: sceneURL)!
        referenceNode.load()
        return referenceNode
    }
    
    func transformMatrix(_ matrix: simd_float4x4, _ originLocation: CLLocation, _ driverLocation: CLLocation, _ distance: Float) -> simd_float4x4 {
        let bearing = bearingBetweenLocations(originLocation, driverLocation)
        let rotationMatrix = rotateAroundY(matrix_identity_float4x4, Float(bearing))
        let position = vector_float4(0.0, 0.0, -distance, 0.0)
        let translationMatrix = getTranslationMatrix(matrix_identity_float4x4, position)
        let transformMatrix = simd_mul(rotationMatrix, translationMatrix)
        return simd_mul(matrix, transformMatrix)
    }
    
    func bearingBetweenLocations(_ originLocation: CLLocation, _ driverLocation: CLLocation) -> Double {
        let lat1 = originLocation.coordinate.latitude.toRadians()
        let lon1 = originLocation.coordinate.longitude.toRadians()
        let lat2 = driverLocation.coordinate.latitude.toRadians()
        let lon2 = driverLocation.coordinate.longitude.toRadians()
        let longitudeDiff = lon2 - lon1
        let y = sin(longitudeDiff) * cos(lat2);
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(longitudeDiff);
        return atan2(y, x)
    }
    
    func rotateAroundY(_ matrix: simd_float4x4, _ degrees: Float) -> simd_float4x4 {
        var matrix = matrix
        matrix.columns.0.x = cos(degrees)
        matrix.columns.0.z = -sin(degrees)
        matrix.columns.2.x = sin(degrees)
        matrix.columns.2.z = cos(degrees)
        return matrix.inverse
    }
    
    func getTranslationMatrix(_ matrix: simd_float4x4, _ translation : vector_float4) -> simd_float4x4 {
        var matrix = matrix; matrix.columns.3 = translation
        return matrix
    }
    
    @IBAction func viewInMapsButton(_ sender: Any) {
        // handle false from openInMaps call ........................................
        if let sn = selectedNode, let po = getPlaceObject(for: sn) {
            po.mapItem.openInMaps()
        }
    }
  
    @IBAction func favoritePushed(_ sender: Any) {
        if let sn = selectedNode, let po = getPlaceObject(for: sn) {
            if po.loadedAs == .mySpot || po.loadedAs == .receivedSpot {
                performSegue(withIdentifier: SegueIdentifiers.menuSegue.rawValue, sender: po)
                return
            }
            po.favorited = !po.favorited;
            var newColor = UIColor(named: "favoriteColor")!
            if !po.favorited {
                switch po.loadedAs {
                case .generalPlace, .favorite: newColor = UIColor(named: "generalPlaceColor")!
                case .searchResult: newColor = UIColor.orange
                case .mySpot: newColor = UIColor(named: "mySpotColor")!
                case .receivedSpot: newColor = UIColor(named: "receivedSpotColor")!
                }
            }
            sn.childNode(withName: "sign", recursively: true)?.removeFromParentNode()
            sn.geometry?.firstMaterial?.diffuse.contents = newColor
            sn.enumerateChildNodes { (n, _) in
                n.geometry?.firstMaterial?.diffuse.contents = newColor
            }
            sn.addChildNode(displayPlane(po.mapItem.name ?? "unknown", color: newColor, iconType: po.iconType))
            favoriteButton.setTitle(po.favorited ? "♥︎":"♡", for: .normal)
            DataStore.shared.mapDisplayDelegate?.didFetchPlaces(recenter: false)
        }
    }
    
    func errorAlert(err: Errors, msg: String? = nil) {
        dismiss(animated: false)  // is this ok???????
        var message = err.errorText
        if let m = msg { message += "\n\n" + m }
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("BUTTON_OK", comment: "OK alert button"), style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    func sendEmail() {
        dismiss(animated: false, completion: nil)
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.setToRecipients([SUPPORT_EMAIL])
            mail.setSubject(EMAIL_SUBJECT)
            mail.mailComposeDelegate = self
            present(mail, animated: true)
        } else {
            errorAlert(err: Errors.noEmail)
        }
    }
}
// MARK: - DataStore Delegate
extension ViewController: DataStoreDelegate {
    func didFetchPlaces() {
        DataStore.shared.customSerialQueue.async {
            self.performRenderPlaces()
        }
        if DataStore.shared.displayingSearchResults {
            DispatchQueue.main.async {
                self.errorAlert(err: Errors.displaySearch)
            }
        }
    }
    
    func addedReceivedSpot(bP: BGFPlaceItem) {
        print("BGF --- top of added received spot") // need to render new received spot ???????????????
        performSegue(withIdentifier: SegueIdentifiers.menuSegue.rawValue, sender: bP)
    }
}

// MARK: - Popover Delegate
extension ViewController: UIPopoverPresentationControllerDelegate {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let pc = segue.destination.popoverPresentationController {
            pc.delegate = self
            pc.backgroundColor = UIColor(named: "boneColor")!
        }
        if let vc = segue.destination as? UINavigationController, let vc2 = vc.topViewController as? MainMenuVC, let bI = sender as? BGFPlaceItem {
            print("BGF --- prepare mainmenuvc in viewcontroller")
            vc2.bgfItem = bI
        }
        if segue.identifier == SegueIdentifiers.menuSegue.rawValue {
            if let vc = segue.destination as? UINavigationController, let vc2 = vc.topViewController as? MainMenuVC, let s = sender as? String, s == "store" {
                vc2.goToStore = true
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}

// MARK: - Location Manager Delegate
extension ViewController: LocationManagerDelegate {
    func didUpdateCurrentLocation() {
        print("BGF --- did update current location called")
        DispatchQueue.main.async {
            self.createSession()
        }
    }
    
    func statusUpdate(message: String) {
        statusMessagesArray[0] = message
    }
    
    func headingUpdate(message: String) {
        statusMessagesArray[2] = message
    }
}

// MARK: - Mail Compose Delegate
extension ViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        print("BGF --- mail delegate called")
        controller.dismiss(animated: true, completion: nil)
    }
}
