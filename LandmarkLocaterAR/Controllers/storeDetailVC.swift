//
//  storeDetailVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 3/30/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import StoreKit

class storeDetailVC: UIViewController {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productButton: UIButton!
    var product: SKProduct!
    private lazy var priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = product.localizedTitle
        if let iName = product.productIdentifier.components(separatedBy: ".").last {
            productImageView.image = UIImage(named: iName + "Image")
        }
        if MySpotsProducts.store.isPurchased(product.productIdentifier) {
            productButton.backgroundColor = UIColor.green
            productButton.setTitle("Purchased ✔︎", for: .normal)
        } else {
            productButton.backgroundColor = UIColor.blue
            priceFormatter.locale = product.priceLocale
            if let priceText = priceFormatter.string(from: product.price) {
                productButton.setTitle("Buy " + priceText, for: .normal)
            } else {
                productButton.setTitle("Not available", for: .normal)
            }
        }
    }
    
    @IBAction func productButtonTapped(_ sender: Any) {
        if !MySpotsProducts.store.isPurchased(product.productIdentifier) {
            navigationController?.popViewController(animated: false)
            MySpotsProducts.store.buyProduct(product: product)
        }
    }
    
}
