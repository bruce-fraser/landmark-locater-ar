//
//  AddSpotVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/16/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import MapKit

class AddSpotVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var pickerControl: UIPickerView!
    @IBOutlet weak var switchControl: UISwitch!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var rightBarButton: UIBarButtonItem!
    
    var isAddSpot = true
    var sortedIconTypes = BGFPlaceItem.IconType.allCases
    
    override func viewDidLoad() {
        super.viewDidLoad(); print("BGF --- add spot view did load")
        navigationController?.navigationBar.barTintColor = UIColor(named: "boneColor")!
        pickerControl.dataSource = self
        pickerControl.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nameField.text = ""
        addButton.isEnabled = false; addButton.alpha = 0.3
        if !isAddSpot { ////// possibly do opposite for addspot vc
            title = "Share Spot";
            rightBarButton.isEnabled = false
        }
        sortedIconTypes = BGFPlaceItem.IconType.allCases.filter { Set(FREE_MODELS).contains($0.rawValue) || MySpotsProducts.store.isPurchased("com.brucefraser.LandmarkLocaterAR.iap." + $0.rawValue) }.sorted(by: <)
        let remainingIconTypes = BGFPlaceItem.IconType.allCases.filter { !Set(FREE_MODELS).contains($0.rawValue) && !MySpotsProducts.store.isPurchased("com.brucefraser.LandmarkLocaterAR.iap." + $0.rawValue) }.sorted(by: <)
        sortedIconTypes.append(contentsOf: remainingIconTypes)
        pickerControl.reloadAllComponents()
        print("BGF --- add spot viewwillappear")
    }
    
    override func viewWillLayoutSubviews() {
         super.viewWillLayoutSubviews()
        navigationController?.isNavigationBarHidden = false
        preferredContentSize = CGSize(width: 300, height: 310)
        if !isAddSpot {
//            view.backgroundColor = UIColor(named: "turquoiseGreenColor")!
//            pickerControl.backgroundColor = UIColor(named: "boneColor")!
            addButton.setTitle("↑", for: .normal)
//            addButton.backgroundColor = UIColor(named: "boneColor")!
        }
        print("BGF --- add spot viewwilllayoutsubviews")
    }
    
    @IBAction func addButtonTapped(_ sender: Any) { 
        let idx = pickerControl.selectedRow(inComponent: 0)
        let iconType = sortedIconTypes[idx]
        
        // Handle icons not yet bought
        let iconName = iconType.rawValue
        if !Set(FREE_MODELS).contains(iconName) && !MySpotsProducts.store.isPurchased("com.brucefraser.LandmarkLocaterAR.iap." + iconName) {
            displayBuyAlert()
            return
        }
        
        let showSign = switchControl.isOn; let weakIsAdd = isAddSpot
        guard let name = nameField.text else { return }
        let appD = UIApplication.shared.delegate as! AppDelegate; let mainVC = appD.window?.rootViewController as! ViewController
        // set hud message; end hud message in addMySpot on main thread
        DispatchQueue.global(qos: .userInitiated).async {
            var err: Errors?
            
            if let loc = LocationManager.shared.getLiveLocation() {
                // set error optional here
                print("BGF --- add spot loc accuracy: \(loc.horizontalAccuracy), altitude: \(loc.altitude)")
                let pM = MKPlacemark(coordinate: loc.coordinate)
                let mI = MKMapItem(placemark: pM)
                mI.name = name; let lType = weakIsAdd ? BGFPlaceItem.LoadedType.mySpot: BGFPlaceItem.LoadedType.receivedSpot
                let placeItem = BGFPlaceItem(loadedAs: lType, isFavorited: false, with: mI, showSign: showSign, iconType: iconType, altitude: loc.altitude)
               
                if weakIsAdd {
                     mainVC.addMySpot(spot: placeItem)
                } else {
                    print("BGF --- top of share spot")
                    let bgfJI = BGFJSONItem(bgfPlaceItem: placeItem)
                    if let data = try? JSONEncoder().encode(bgfJI), let docs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                        let path = docs.appendingPathComponent("/sharespot.mspt")
                        do {
                            try data.write(to: path, options: .atomicWrite)
                            let intro = "I sent you a spot.  Click to open in My Spots AR (only on iPhone here: \(APP_STORE_LINK).  Then scroll to 'Copy to My Spots AR'."
                            let items: [Any] = [intro, path]
                            DispatchQueue.main.async {
                                // turn off HUD
                                let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                                ac.excludedActivityTypes = [UIActivity.ActivityType.message]
                                mainVC.present(ac, animated: true)
                            }
                        } catch {
                            err = Errors.noWriteObjectToFile
                        }
                    } else {
                        err = Errors.noPrepareObject
                    }
                }
            } else {
                err = Errors.noLiveLocation
            }
            if let e = err {
                DispatchQueue.main.async {
                    mainVC.errorAlert(err: e)
                }
            }
        }
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortedIconTypes.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return BGFPlaceItem.IconType.allCases[row].rawValue
//    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return pickerView.bounds.height / 3
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let iView = UIImageView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.height / 3, height: pickerView.bounds.height / 3))
        let iName = sortedIconTypes[row].rawValue
        iView.image = UIImage(named: iName + "Image")
        if !Set(FREE_MODELS).contains(sortedIconTypes[row].rawValue) && !MySpotsProducts.store.isPurchased("com.brucefraser.LandmarkLocaterAR.iap." + iName) {
            let buyLabel = UILabel(frame: iView.bounds); buyLabel.textAlignment = .center
            buyLabel.adjustsFontSizeToFitWidth = true; buyLabel.text = "Buy"; buyLabel.textColor = UIColor.red
            iView.addSubview(buyLabel)
        }
        return iView
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func textFieldChanged(_ sender: Any) {
        if let name = nameField.text, name.count > 0 {
            addButton.isEnabled = true; addButton.alpha = 1.0
        } else {
            addButton.isEnabled = false; addButton.alpha = 0.3
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.manageSpotsSegue.rawValue, let vc = segue.destination as? ManageMySpotsTVC {
            vc.receivedSpotsController = false
            vc.bgfItem = nil
        }
    }
    
    func displayBuyAlert() {
        guard let vc = UIApplication.shared.keyWindow?.rootViewController as? ViewController else { return }
        let bAlert = UIAlertController(title: "Buy icon?", message: "Do you want to buy this icon?", preferredStyle: .alert)
        let noAction = UIAlertAction(title: "No", style: .cancel) { [weak self] (action) in
            self?.pickerControl.selectRow(0, inComponent: 0, animated: true)
        }
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            vc.dismiss(animated: false); vc.performSegue(withIdentifier: SegueIdentifiers.menuSegue.rawValue, sender: "store")
        }
        bAlert.addAction(noAction); bAlert.addAction(yesAction)
        present(bAlert, animated: true, completion: nil)
    }
}
