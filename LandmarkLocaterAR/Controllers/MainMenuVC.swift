//
//  MainMenuVC.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/10/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit

class MainMenuVC: UIViewController {

    var bgfItem: BGFPlaceItem?
    var goToStore: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if goToStore {
            performSegue(withIdentifier: SegueIdentifiers.storeSegue.rawValue, sender: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
        print("BGF --- main menu view will appear")
        if bgfItem != nil {
            if bgfItem!.loadedAs == .receivedSpot {
                performSegue(withIdentifier: SegueIdentifiers.receivedSpotsSegue.rawValue, sender: nil)
            } else {
                performSegue(withIdentifier: SegueIdentifiers.manageSpotsSegue2.rawValue, sender: nil)
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
         super.viewWillLayoutSubviews()
        navigationController?.isNavigationBarHidden = true
        navigationController?.preferredContentSize = CGSize(width: 190, height: 195)
        preferredContentSize = CGSize(width: 190, height: 195)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.shareSpotSegue.rawValue, let vc = segue.destination as? AddSpotVC {
            vc.isAddSpot = false
        }
        if segue.identifier == SegueIdentifiers.addSpotSegue.rawValue, let vc = segue.destination as? AddSpotVC {
             vc.isAddSpot = true
        }
        if segue.identifier == SegueIdentifiers.receivedSpotsSegue.rawValue, let vc = segue.destination as? ManageMySpotsTVC {
            vc.receivedSpotsController = true
            if let bI = bgfItem {
                vc.bgfItem = bI
                bgfItem = nil
            } else {
                vc.bgfItem = nil
            }
        }
        if segue.identifier == SegueIdentifiers.manageSpotsSegue2.rawValue, let vc = segue.destination as? ManageMySpotsTVC {
            vc.receivedSpotsController = false
            if let bI = bgfItem {
                vc.bgfItem = bI
                bgfItem = nil
            } else {
                vc.bgfItem = nil
            }
        }
    }
}
