//
//  AppDelegate.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 12/24/18.
//  Copyright © 2018 Bruce Fraser. All rights reserved.
//

import UIKit; import StoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UINavigationBar.appearance().barTintColor = UIColor(named: "boneColor")!
        UINavigationBar.appearance().isOpaque = true
        UINavigationBar.appearance().tintColor = UIColor(named: "sandyBrownColor")!
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "graniteGrayColor")!]
        let firstTime = !UserDefaults.standard.bool(forKey: "firstTime")
        print("BGF --- did finish launching first time: \(firstTime)")
        if firstTime {
            UserDefaults.standard.set(true, forKey: "showGeneralPlacesFlag")
            UserDefaults.standard.set(["bank", "restaurant", "school", "coffee", "gas"], forKey: "searchTermsArray")
            UserDefaults.standard.set(true, forKey: "firstTime")
        }
        SKPaymentQueue.default().add(self)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        print("BGF --- application will resign active.")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        LocationManager.shared.stopUpdating(); print("BGF --- stop updating in background")
        DataStore.shared.storeItems()
        if let vc = window?.rootViewController as? ViewController {
            vc.sceneView.session.pause()
        }
        print("BGF --- application did enter background")
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        guard url.pathExtension == "mspt" else { print("BGF --- not .mspt"); return false }
        DataStore.shared.processNewItems(nil, of: .receivedSpot, url: url)
        print("BGF --- processed .mspt in open url")
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        LocationManager.shared.requestLocationUpdates()     
        print("BGF --- will enter foreground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("BGF --- application did become active")
    }

    func applicationWillTerminate(_ application: UIApplication) {}
}

extension AppDelegate: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased, .restored: completeTransaction(transaction)
            case .failed: failedTransaction(transaction)
            default: print("BGF --- transaction type not handled")
            }
        }
    }
    
    fileprivate func completeTransaction(_ transaction: SKPaymentTransaction) {
        deliverPurchaseNotification(for: transaction.payment.productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    fileprivate func failedTransaction(_ transaction: SKPaymentTransaction) {
        if let transactionError = transaction.error as NSError?,
            let localizedDescription = transaction.error?.localizedDescription,
            transactionError.code != SKError.paymentCancelled.rawValue {
//            if let vc = window?.rootViewController as? ViewController {
//                vc.dismiss(animated: false, completion: nil)
//                vc.errorAlert(err: Errors.storeError, msg: localizedDescription)
//            }
            print("BGF --- Store Transaction error: \(localizedDescription)")
        }
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    fileprivate func deliverPurchaseNotification(for identifier: String?) {
        guard let identifier = identifier else { return }
        NotificationCenter.default.post(name: .purchaseNotification, object: identifier)
        MySpotsProducts.handlePurchase(purchaseIdentifier: identifier)
    }
}

extension Notification.Name {
    static let purchaseNotification = Notification.Name("PurchaseNotification")
}
