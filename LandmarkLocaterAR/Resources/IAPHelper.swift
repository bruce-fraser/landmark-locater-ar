//
//  IAPHelper.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 3/31/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import Foundation; import StoreKit

public typealias ProductsRequestCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> Void

class IAPHelper: NSObject {
    
    public var purchasedProducts = Set<String>()
    private let productIdentifiers: Set<String>
    private var productsRequest: SKProductsRequest?
    private var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
    
    public func isPurchased(_ productIdentifier: String) -> Bool {
        return purchasedProducts.contains(productIdentifier)
    }
    
    public func restorePurchases() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    init(productIDs: Set<String>) {
        productIdentifiers = productIDs
        purchasedProducts = Set(productIDs.filter {
            UserDefaults.standard.bool(forKey: $0)
        })
        super.init()
    }
    
    func buyProduct(product: SKProduct) {
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    
    func requestProducts(completionHandler: @escaping ProductsRequestCompletionHandler) {
        productsRequest?.cancel()
        productsRequestCompletionHandler = completionHandler
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest?.delegate = self
        productsRequest?.start()
    }
}

extension IAPHelper: SKProductsRequestDelegate {
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        productsRequestCompletionHandler?(true, response.products)
        productsRequestCompletionHandler = .none
        productsRequest = .none
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        productsRequestCompletionHandler?(false, nil)
        productsRequestCompletionHandler = .none
        productsRequest = .none
    }
}
