//
//  Utilities.swift
//

import Foundation; import MapKit; import CoreLocation; import SceneKit; import Contacts

// MARK: - Contants
let WEB_SITE_URL = "https://myspotsar.com"
let APP_STORE_LINK = "https://itunes.apple.com/app/id1454687389?mt=8"
let APP_STORE_REVIEW_LINK = "https://itunes.apple.com/app/id1454687389?action=write-review"
let SUPPORT_EMAIL = "bfras33@gmail.com"
let APP_VERSION = "version 2.0"
let EMAIL_SUBJECT = "My Spots AR (from app \(APP_VERSION)"
let FREE_MODELS = ["sphere", "cube", "shark", "smirk", "love"]

// MARK: - Enums
enum SegueIdentifiers: String {
    case menuSegue
    case shareSpotSegue
    case receivedSpotsSegue
    case addSpotSegue
    case manageSpotsSegue
    case editPlaceSegue
    case manageSpotsSegue2
    case settingsSegue
    case searchTermsSegue
    case storeSegue
    case storeDetailSegue
}

enum UserDefaultsKeys: String {
    case processComletedCountKey
    case lastVersioPromptedForReviewKey
}

enum Errors {
    case noLiveLocation
    case noPrepareObject
    case noWriteObjectToFile
    case mySpotAdded
    case noEmail
    case displaySearch
    case storeError
    
    var errorText: String {
        switch self {
        case .noLiveLocation: return "Not able to get current location. Try again later."
        case .noPrepareObject, .noWriteObjectToFile: return "Not able to prepare spot for sending."
        case .mySpotAdded: return "Added spot near you."
        case .noEmail: return "Sorry.  Can't bring up email form."
        case .displaySearch: return "Displaying results that are within range. See map for all results."
        case .storeError: return "Problem processing product request."
        }
    }
}

// MARK: - Functions
func *(vector:SCNVector3, multiplier:SCNFloat) -> SCNVector3 {
    return SCNVector3(vector.x * multiplier, vector.y * multiplier, vector.z * multiplier)
}

func *=( vector: inout SCNVector3, multiplier:SCNFloat) {
    vector = vector * multiplier
}

// MARK: - Extensions
extension FloatingPoint {
    func toRadians() -> Self {
        return self * .pi / 180
    }
    func toDegrees() -> Self {
        return self * 180 / .pi
    }
}

extension CLLocation {
    static func ==(lhs: CLLocation, rhs: CLLocation) -> Bool {
        return lhs.coordinate.latitude == rhs.coordinate.latitude && lhs.coordinate.longitude == rhs.coordinate.longitude
    }
}

extension MKMapItem {
    var clLocation: CLLocation {
        return CLLocation(latitude: self.placemark.coordinate.latitude, longitude: self.placemark.coordinate.longitude)
    }
    
    static func ==(lhs: MKMapItem, rhs: MKMapItem) -> Bool {
        return lhs.name == rhs.name && lhs.clLocation == rhs.clLocation
    }
}

extension MKPlacemark {
    var formattedAddress: String? {
        guard let postalAddress = postalAddress else { return nil }
        return CNPostalAddressFormatter.string(from: postalAddress, style: .mailingAddress).replacingOccurrences(of: "\n", with: " ")
    }
}

extension CGContext {
    func drawLinearGradient(in rect: CGRect, startingWith startColor: CGColor, finishingWith endColor: CGColor) {
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let locations = [0.0, 1.0] as [CGFloat]
        let colors = [startColor, endColor] as CFArray
        guard let gradient = CGGradient(colorsSpace: colorSpace, colors: colors, locations: locations) else { return }
        let startPoint = CGPoint(x: rect.midX, y: rect.minY)
        let endPoint = CGPoint(x: rect.midX, y: rect.maxY)
        saveGState()
        addRect(rect)
        clip()
        drawLinearGradient(gradient, start: startPoint, end: endPoint, options: CGGradientDrawingOptions())
        restoreGState()
    }
}

extension UISearchBar {
    func configure() {
        if let searchField = self.value(forKey: "searchField") as? UITextField {
            
            if let iconView = searchField.leftView as? UIImageView {
                iconView.tintColor = UIColor.white
            }
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            UIImageView.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = UIColor.white
            searchField.setNeedsDisplay()
        }
    }
}

extension UIColor {
    func darkerColor(percent: Double) -> UIColor {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        let factor = CGFloat((100 - percent) / 100)
        if getRed(&r, green: &g, blue: &b, alpha: &a) {
            return UIColor(red: r * factor, green: g * factor, blue: b * factor, alpha: a)
        } else {
            return UIColor.red
        }
    }
}

extension SCNVector3 {
    func horizontalDistance(to: SCNVector3) -> Float {
        let xDiff = to.x - x; let zDiff = to.z - z
        let dist = sqrtf(xDiff * xDiff + zDiff * zDiff)
        return dist
    }
}
