//
//  MapView.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/11/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit

class MapView: UIView {
    override func layoutSubviews() {
        var path: UIBezierPath?
        let rad = max(bounds.size.width, bounds.size.height)
        if traitCollection.verticalSizeClass == .compact {
            path = UIBezierPath(arcCenter: CGPoint(x: rad, y: bounds.size.height/2), radius: rad, startAngle: .pi/2, endAngle: .pi * 3/2, clockwise: true)
        } else {
            path = UIBezierPath(arcCenter: CGPoint(x: bounds.size.width/2, y: rad), radius: rad, startAngle: 0.0, endAngle: .pi, clockwise: false)
        }
        if let p = path {
            let maskLayer = CAShapeLayer()
            maskLayer.path = p.cgPath
            self.layer.mask = maskLayer
        }
    }
}
