//
//  BGFPlaceView.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/13/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import MapKit

class BGFPlaceView: MKMarkerAnnotationView {

    override var annotation: MKAnnotation? {
        willSet {
            if let placeAnnotation = newValue as? BGFPlaceItem {
                if placeAnnotation.favorited {
                    glyphText = "❤️"; markerTintColor = UIColor.red; displayPriority = .required
                } else {
                    switch placeAnnotation.loadedAs {
                    case .searchResult: glyphText = "🔍"; markerTintColor = UIColor.orange; displayPriority = .required
                    case .generalPlace, .favorite: glyphText = nil; markerTintColor = UIColor.blue; displayPriority = .required
                    case .mySpot: glyphText = nil; markerTintColor = placeAnnotation.loadedAs.color; displayPriority = .required
                    case .receivedSpot: glyphText = nil; markerTintColor = placeAnnotation.loadedAs.color; displayPriority = .required
                    }
                }
                canShowCallout = true
                let infoButton = UIButton(type: .detailDisclosure)
                rightCalloutAccessoryView = infoButton
            }
        }
    }
}
