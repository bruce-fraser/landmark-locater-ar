//
//  CellBackgroundView1.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 3/7/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit

class CellBackgroundView1: UIView {

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        let col = UIColor(named: "boneColor")!
        let backgroundRect = bounds
        context.drawLinearGradient(in: backgroundRect, startingWith: col.cgColor, finishingWith: col.darkerColor(percent: 15).cgColor)
    }

}
