//
//  ClusterView.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/15/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit; import MapKit

class ClusterView: MKMarkerAnnotationView {

    override var annotation: MKAnnotation? {
        willSet {
            markerTintColor = UIColor.brown
        }
    }
}
