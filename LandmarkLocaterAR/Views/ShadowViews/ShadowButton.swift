//
//  ShadowButton.swift
//  LandmarkLocaterAR
//
//  Created by Bruce Fraser on 2/19/19.
//  Copyright © 2019 Bruce Fraser. All rights reserved.
//

import UIKit

class ShadowButton: UIButton {

//    override func layoutSubviews() {
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//        layer.shadowOpacity = 0.4
//        layer.shadowRadius = 1.0
//        layer.masksToBounds = false
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 1.0
        layer.masksToBounds = false
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 1.0
        layer.masksToBounds = false
    }
}
